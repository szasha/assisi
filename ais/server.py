#!/usr/bin/env python
# -*- coding: utf-8 -*-
### The web server part
import sys, time, pathlib, weakref, traceback
import concurrent, asyncio, aiohttp, aiohttp_autoreload, aiohttp_jinja2, jinja2
from geopy.distance import geodesic as geo_distance

import log
log.init_logging()

import settings
from server_util import render, redirect, schedule_background
import vessels, gps




# Background jobs -->

def start_background(app):
    """Watch vessels in background, ws broadcast new sightings.
    """

    position        = app['position']        = [settings.LAT_HOME, settings.LON_HOME]
    current_vessels = app['current_vessels'] = {}


    # Start process to read gps position

    if settings.GPS_DEVICE and settings.GPS_BAUD:
        async def callback_position(pos):
            position[0] = pos[0]
            position[1] = pos[1]

        async def job():
            await gps.from_serial(
                    device   = settings.GPS_DEVICE,
                    baud     = settings.GPS_BAUD,
                    callback = callback_position)

        schedule_background(app, 'get_gps', job)



    # Start the process to read the ais device and propagate vessel sightings


    async def callback_vessel(vessel):
        vessel['ts'] = int(time.time()) # Lets not rely on any ais timestamps, this is as good as any

        vessel['distance'] = None
        lat, lon = position
        vessel_lat, vessel_lon = vessel.get('lat'), vessel.get('lon')
        if lat and lon and vessel_lat and vessel_lon:
            try:
                distance = geo_distance((lat, lon), (vessel_lat, vessel_lon))
                vessel['distance'] = int(distance.m)
            except ValueError:
                pass

        current_vessels[vessel['mmsi']] = vessel
        await ws_broadcast(app, {'seen': [vessel,],})

    async def job():
        await vessels.from_serial(
                device   = settings.AIS_DEVICE,
                baud     = settings.AIS_BAUD,
                callback = callback_vessel)

    schedule_background(app, 'find_vessels', job)


    # Start the process to filter vessels not seen since settings.VESSEL_MAXAGE
    # and propagate them as outdated

    async def job():
        while 1:
            try:
                ts = time.time()
                mmsis = [v['mmsi'] for v in current_vessels.values()
                    if ts - v['ts'] > settings.VESSEL_MAXAGE]
                if mmsis:
                    LOG.debug('Discard %i outdated sightings', len(mmsis))
                    for mmsi in mmsis:
                        current_vessels.pop(mmsi)
                    await ws_broadcast(app, {'timeout': mmsis,})

            except concurrent.futures._base.CancelledError:
                LOG.debug('Cancelled')
                break

            except Exception as e:
                LOG.exception(e)

            await asyncio.sleep(10)

    schedule_background(app, 'clean_vessels', job)

# Background jobs <--


# Websockets -->

async def handle_ws(request):
    """Handle websocket listeners.
    """
    mode = request.match_info['mode']
    remote_ip = request.headers.get('X-FORWARDED-FOR', request.remote)

    LOG.info('WS connection from %s, mode %s', remote_ip, mode)

    if not mode in ['notify', 'update',]:
        raise aiohttp.web.HTTPNotFound()

    clients = request.app['clients_%s' % mode]

    client = aiohttp.web.WebSocketResponse()
    await client.prepare(request)
    clients.add(client)

    ws_id = hash(client) # For logging
    LOG.info('WS %s connected, mode %s clients now: %i', ws_id, mode, len(clients))

    if mode == 'update':
        # For a start send current data, then only updates
        vessels = request.app['current_vessels'].values()
        vessels = [v for v in vessels]
        if DEBUG:
            vessel_count = len(vessels)
        await client.send_json({'seen': vessels,})

    try:
        # We don't expect any text from client,
        # just keeping the client in a loop so we can call it back.
        async for msg in client:

            if msg.type == aiohttp.WSMsgType.text:
                # Log and forget.
                LOG.debug('WS %i text %s', ws_id, msg.data)
                await client.close(
                    code = aiohttp.WSCloseCode.UNSUPPORTED_DATA,
                    message = 'Server didn\'t expect any text from you')
                break

            elif msg.type == aiohttp.WSMsgType.CLOSE:
                # Client is finished.
                # Removed from list of ws clients in finally statement.
                LOG.info('WS %i closing', ws_id)
                await client.close(
                    message = 'You closed the connection.')
                break

            elif msg.type == aiohttp.WSMsgType.ERROR:
                # Client is in trouble, lets finish it.
                # Removed from list of ws clients in finally statement.
                LOG.exception('WS %i error %s', ws_id, request.exception())
                await client.close(
                    message = 'Some error occurred with receiving your messages')
                break

            else:
                # Something else. Should we close here?
                LOG.warning('WS %i unimplemented message type %s', ws_id, msg.type)

    except Exception as e:
        LOG.exception(e)
        if DEBUG:
            traceback.print_exc()
        # On error we just disconnect the ws client. It has to reconnect itself.

    finally:
        # Remove the ws client from listeners due to error or closed connection.
        clients.discard(client)
        LOG.info('WS %i disconnected, mode %s clients now: %i', ws_id, mode, len(clients))




async def ws_broadcast(app, data):
    """Broadcast passed data to all connected websockets 'update' clients.
    Notify all connected websockets 'notify' clients about new data.
    """
    async def send(clients, data):
        for client in [client for client in clients]:
            try:
                await client.send_json(data)
            except Exception as e:
                LOG.exception(e)
                try:
                    await client.close(
                        message = 'Some error occurred while sending you data')
                except Exception as e:
                    LOG.exception(e)
                finally:
                    clients.discard(client)

    clients = app['clients_%s' % 'update']
    await send(clients, data)

    clients = app['clients_%s' % 'notify']
    await send(clients, {'message': 'updated',})




async def ws_on_shutdown(app):
    """Be nice - on server shutdown notify all connected websockets.
    """
    for mode in ['notify', 'update',]:
        clients = app['clients_%s' % mode]
        LOG.info('Server shutdown, notifying %i %s clients', len(clients), mode)
        for client in  [client for client in clients]:
            await client.close(
                code = aiohttp.WSCloseCode.GOING_AWAY,#SERVICE_RESTART,
                    message = 'Server closing')
            clients.discard(client)

# Websockets <--




# Funtions exposed as REST API -->
## Don't forget to sanitize input

async def get_vessels(request):
    """Get vessels as json.
    """
    vessels = request.app['current_vessels']
    return aiohttp.web.json_response(vessels)




async def get_geo(request):
    """Get vessels as geojson.
    """
    vessels = request.app['current_vessels']
    data = [
         {
             'type': 'Feature',
             'geometry': {
                 'type': 'Point',
                 'coordinates': [v['lon'], v['lat']],
             },
             'properties': v,
         } for v in vessels.values() if 'lat' in v and 'lon' in v
    ]
    data = {
        'type': 'FeatureCollection',
        'features': data,
    }
    return aiohttp.web.json_response(data)

# Funtions exposed as REST API <--




def main(**server_args):
    """Everything starts here.
    """
    LOG.info('Server starting %s', str(server_args)[1:-1])

    # Filesystem paths to static files and to templates
    root_path        = pathlib.Path(__file__).parent
    static_root_path = root_path.joinpath('static')
    static_root_url  = settings.STATIC_ROOT_URL#root_path.joinpath('static')
    templates_path   = root_path.joinpath('templates')

    app = aiohttp.web.Application()
    app.on_shutdown.append(ws_on_shutdown)

    app['name'] = settings.NAME
    for mode in ['notify', 'update',]:
        app['clients_%s' % mode] = weakref.WeakSet()

    # Setup template engine
    aiohttp_jinja2.setup(app,
            trim_blocks        = True,
            lstrip_blocks      = True,
            auto_reload        = DEBUG,
            context_processors = [aiohttp_jinja2.request_processor,],
            loader             = jinja2.FileSystemLoader(str(templates_path)))

    # For static url template tag
    app['static_root_url'] = static_root_url

    # Route to serve static files
    app.router.add_static(
        static_root_url, static_root_path, name = 'static')#, show_index = True)

    # URL -> handler mapping
    app.add_routes([

        # API
        aiohttp.web.get(r'/ais/ws/{mode}', handle_ws,   name = 'ws'),  # Serve websocket clients
        aiohttp.web.get( '/ais/get',       get_vessels, name = 'get'), # Get vessels as json
        aiohttp.web.get( '/ais/geo',       get_geo,     name = 'geo'), # Get vessels as geojson

        # Pages
        aiohttp.web.get('/ws/dump',  render('wsdump.html'), name = 'wsdump'), # For websocket testing
        aiohttp.web.get('/ais/map',  render('map.html'),    name = 'map'),    # Map
        aiohttp.web.get('/ais/list', render('list.html'),   name = 'list'),   # A selfupdating list

        # Catch all
        aiohttp.web.get('/{tail:.*}', redirect('list'), name = 'catchall'), # Redirect the rest to the listing
    ])

    if DEBUG:
        # Auto reload code, restart server on code change
        aiohttp_autoreload.start()

    # Start background jobs
    start_background(app)

    # No access logging
    #server_args['access_log'] = None

    if not 'port' in server_args:
        server_args['port'] = settings.HTTP_PORT
    if not 'host' in server_args:
        server_args['host'] = settings.HTTP_HOST

    # Start server, blocking
    aiohttp.web.run_app(app, **server_args)
    LOG.info('Server stopped')


if __name__ == '__main__':
    kwargs = {}
    for arg in sys.argv[1:]:
        key, val = arg.split('=')
        kwargs[key] = val
    main(**kwargs)
