# -*- coding: utf-8 -*-
import sys, signal, traceback
import asyncio, aioserial
from pynmea2 import parse as parse_nmea
from pynmea2.nmea import ParseError as NmeaParseError
from serial.serialutil import SerialException
from concurrent.futures._base import CancelledError


import log
log.init_logging()


serial = None
running = False


def stop():
    global running
    LOG.info('stop')

    running = False
    _close()


def _close():
    global serial
    LOG.info('close')

    s, serial = serial, None
    if s:
        LOG.info('Close  serial device')
        try:
            s.close()
            LOG.info('Closed serial device')
        except Exception as e:
            LOG.warning('Problem with closing serial device')
            LOG.exception(e)


async def from_serial(device, baud, callback):
    """Reads GPS NMEA from a serial device and pass the parsed positions to the callback.
    """
    global serial, running

    assert(device)
    assert(baud)
    assert(callback)

    LOG.info('Reading from serial device %s %s', device, baud)

    running = True

    def on_shutdown(signum, frame):
        LOG.info('on_shutdown')
        stop()
    for sig in (signal.SIGINT, signal.SIGTERM, signal.SIGABRT):
        LOG.debug('Register for exit on os signal %s', sig)
        signal.signal(sig, on_shutdown)

    while running:
        try:
            if not serial:
                LOG.info('Open   serial device %s %s', device, baud)
                serial = aioserial.AioSerial(port = device, baudrate = baud, timeout = 5)
                LOG.info('Opened serial device')
            line = await serial.readline_async()

        except CancelledError:
            LOG.debug('CancelledError')
            stop()
            continue

        except SerialException as e:
            LOG.warning('Serial problem')
            LOG.exception(e)
            _close()
            if running:
                LOG.debug('Taking a nap for 2 seconds before trying again')
                await asyncio.sleep(2)
            continue

        except Exception as e:
            LOG.warning('Some other exception')
            LOG.exception(e)
            if DEBUG:
                traceback.print_exc()
            _close()
            if running:
                LOG.debug('Taking a nap for 2 seconds before trying again')
                await asyncio.sleep(2)
            continue

        if not line:
            LOG.warning('Received empty line from serial')
            continue
        #LOG.debug(line)

        position = None
        try:
            line = line.decode()[:-2]
            message = parse_nmea(line)
            lat, lon = message.latitude, message.longitude
            if lat and lon:
                lat, lon = int(lat * 1000000) / 1000000, int(lon * 1000000) / 1000000
                position = (lat, lon)

        except NmeaParseError:
            continue

        except AttributeError:
            continue

        except Exception as e:
            LOG.exception(e)
            if DEBUG:
                traceback.print_exc()
            continue

        if position:
            try:
                await callback(position)

            except Exception as e:
                if DEBUG:
                    traceback.print_exc()
                LOG.exception(e)

    LOG.info('from_serial returns')




if __name__ == '__main__':
    device, boud = sys.argv[1], int(sys.argv[2])
    async def callback(pos):
        print(pos)
    asyncio.run(from_serial(device, boud, callback))
