# -*- coding: utf-8 -*-
import sys, signal, traceback, json
import asyncio, aioserial, aiofiles
#import pyais as ais
from serial.serialutil import SerialException
from concurrent.futures._base import CancelledError



import log
log.init_logging()


serial = None
running = False


def stop():
    global running
    LOG.info('stop')

    running = False
    _close()


def _close():
    global serial
    LOG.info('close')

    s, serial = serial, None
    if s:
        LOG.info('Close  serial device')
        try:
            s.close()
            LOG.info('Closed serial device')
        except Exception as e:
            LOG.warning('Problem with closing serial device')
            LOG.exception(e)


async def from_serial(device, baud, callback):
    """Reads AIS NMEA from a serial device and pass the parsed vessel to the callback.
    """
    global serial, running

    assert(device)
    assert(baud)
    assert(callback)

    LOG.info('Reading from serial device %s %s', device, baud)

    running = True

    def on_shutdown(signum, frame):
        LOG.info('on_shutdown')
        stop()
    for sig in (signal.SIGINT, signal.SIGTERM, signal.SIGABRT):
        LOG.debug('Register for exit on os signal %s', sig)
        signal.signal(sig, on_shutdown)

    while running:
        try:
            if not serial:
                LOG.info('Open   serial device %s %s', device, baud)
                serial = aioserial.AioSerial(port = device, baudrate = baud, timeout = 5)
                LOG.info('Opened serial device')
            line = await serial.readline_async()

        except CancelledError:
            LOG.debug('CancelledError')
            stop()
            continue

        except SerialException as e:
            LOG.warning('Serial problem')
            LOG.exception(e)
            _close()
            if running:
                LOG.debug('Taking a nap for 2 seconds before trying again')
                await asyncio.sleep(2)
            continue

        except Exception as e:
            LOG.warning('Some other exception')
            LOG.exception(e)
            if DEBUG:
                traceback.print_exc()
            _close()
            if running:
                LOG.debug('Taking a nap for 2 seconds before trying again')
                await asyncio.sleep(2)
            continue

        if not line:
            LOG.debug('Received empty line from serial')
            continue
        #LOG.debug(line)

        try:
            vessel = to_vessel(line)
            if vessel:
                await callback(vessel)

        except Exception as e:
            LOG.exception(e)
            continue

    LOG.info('from_serial returns')




def to_vessel(line):
    """Parse the ais nmea line and return the resulting vessel.
    Catch ais specific exceptions.
    """
    try:
        msg = ais.NMEAMessage(line)
        msg = msg.decode(silent = False)
        msg = msg.to_json()
        msg = json.loads(msg)
        msg = msg['decoded']
        return clean_vessel(msg)

    except ais.exceptions.InvalidNMEAMessageException:
        LOG.warning('Invalid nmea')

    except ais.exceptions.InvalidChecksumException:
        LOG.warning('Invalid checksum')

    except ais.exceptions.UnknownMessageException:
        LOG.warning('Unknown message')


def clean_vessel(vessel):
    """Return dict with only the significant data in passed dict.
    Reduce alt/lon to 5 decimals.
    """
    keys = ['mmsi', 'lat', 'lon', 'shipname', 'callsign', 'radio',]
    vessel = {key: vessel.get(key) for key in keys}

    # lat/lon with 5 decimal places is more then enough, lets cut the rest
    if vessel.get('lat'):
        vessel['lat'] = int(vessel['lat'] * 100000) / 100000
    if vessel.get('lon'):
        vessel['lon'] = int(vessel['lon'] * 100000) / 100000

    return vessel




async def from_file(path, callback):
    """For testing.
    Read ais nmea from a file and pass the parsed vessel to the callback.
    """
    LOG.info('Reading AIS from file %s', path)

    async with aiofiles.open(path, 'rb') as fin:
        async for line in fin:
            vessel = to_vessel(line)
            await callback(vessel)




if __name__ == '__main__':
    device, boud = sys.argv[1], int(sys.argv[2])
    async def callback(pos):
        print(pos)
    asyncio.run(from_serial(device, boud, callback))
