# -*- coding: utf-8 -*-
"""Loging helper.
"""
import os, sys, logging
import inspect


def init_logging(logfile = None, debug = None):
    """Init logging for calling module.
    Attaches a log object 'LOG' to calling module.
    """
    if not logfile:
        logfile = os.getenv('LOGFILE') or None

    if debug is None:
        debug = os.getenv('DEBUG', 'no')
        debug = debug.lower() in ['1', 'true', 'yes',]

    frm = inspect.stack()[1]
    mod = inspect.getmodule(frm[0])
    name = os.path.basename(mod.__file__)[:-3]
    name = name.ljust(7)
    log = logging.getLogger(name)

    mod.DEBUG = debug

    if logfile:
        channel = logging.FileHandler(logfile)
    else:
        channel = logging.StreamHandler(sys.stdout)

    if debug:
        log.setLevel(logging.DEBUG)
        channel.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)
        channel.setLevel(logging.INFO)

    channel.setFormatter(logging.Formatter(
        '%(asctime)s - %(name)-10s - %(levelname)-5s - %(message)s'))

    log.addHandler(channel)
    mod.LOG = log
    mod.LOG.info(
        'Logging for %s initialized, level is %s, logfile is %s',
        name, 'DEBUG' if debug else 'INFO', logfile)
    #mod.LOG.debug('Yes, debug logging works')
    #mod.LOG.warn('Yes, warn logging works')
    #mod.LOG.error('Yes, error logging works')
