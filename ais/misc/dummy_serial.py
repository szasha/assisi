#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Emulate tty with
# socat -d -d pty,raw,echo=0 pty,raw,echo=0
import sys, time, serial


if __name__ == '__main__':

    path, port, delay = sys.argv[1:]
    delay = float(delay)

    device = serial.Serial(port = port, baudrate = 9600)

    n = 0
    try:
        while 1:
            print('Reading from %s, Writing to %s, Delay: %s' % (path, port, delay))
            with open(path, 'rb') as f:
                while 1:
                    line = f.readline()
                    if not line:
                        break
                    device.write(line)
                    n += 1
                    if delay:
                        time.sleep(delay)
            print('Passed %i lines' % n)

    except KeyboardInterrupt:
        pass

    finally:
        print('Passed %i lines' % n)
        device.close()
