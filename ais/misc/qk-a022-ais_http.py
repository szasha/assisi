#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, serial, requests


def main(url, port = '/dev/ttyUSB0', baud = 38400):
    ser = serial.Serial(port, baud, timeout = None)
    for line in ser:
        line = line.decode()
        line = line.strip('\r\n')
        if line.startswith('Quark-elec:'):
            line = line[len('Quark-elec:'):]
        print(line)
        print(requests.post(url = url, data = line).status_code)


if __name__ == '__main__':
    kwargs = {}
    for arg in sys.argv[1:]:
        key, val = arg.split('=', 1)
        kwargs[key] = val
    main(**kwargs)
