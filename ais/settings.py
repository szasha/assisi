# -*- coding: utf-8 -*-
# Settings

NAME = 'AIS'

LAT_HOME = 52.54810
LON_HOME = 13.40789

GPS_DEVICE = '/dev/serial/by-id/usb-Silicon_Labs_CP2104_USB_to_UART_Bridge_Controller_011805DB-if00-port0'
GPS_BAUD   = 4800

AIS_DEVICE = '/dev/serial/by-id/usb-1a86_USB_Serial-if00-port0'
AIS_BAUD   = 9600

VESSEL_MAXAGE = 30 # seconds

HTTP_PORT = 8000
HTTP_HOST = '0.0.0.0'

STATIC_ROOT_URL = '/static'

TEMPLATE_CONTEXT = {
    'name'    : NAME,
    'ws_port' : HTTP_PORT,
}
