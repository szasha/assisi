# -*- coding: utf-8 -*-
### Generic aiohttp stuff
import asyncio, aiohttp, aiohttp_jinja2

import log
log.init_logging()

import settings




def get_template_context(request):
    """Build base for a context to render a template.
    """
    ctx =      {key: val for (key, val) in settings.TEMPLATE_CONTEXT.items()}
    ctx.update({key: val for (key, val) in request.match_info.items()})
    ctx.update(request.rel_url.query)
    return ctx




def render(template):
    """Generate generic GET template handler for passed templates.
    """
    async def handler(request):
        ctx = get_template_context(request)
        return aiohttp_jinja2.render_template(template, request, ctx)
    return handler




def redirect(route):
    """Generate generic redirect handler for passed named route.
    """
    async def handler(request):
        url = request.app.router[route].url_for()
        raise aiohttp.web.HTTPFound(location = url, text = 'Redirect to %s\n' % url)
    return handler




def raise_notfound(text = None):
    """Raise 404, pass text.
    """
    async def handler(request):
        path = request.match_info['tail']
        LOG.info('404: %s', path)
        msg = text if text else 'Not found: %s' % path
        raise aiohttp.web.HTTPNotFound(body = msg.encode())
    return handler




def schedule_background(app, name, job, skip_shutdown = False):
    LOG.info('Schedule background job %s', name)

    async def on_startup(app):
        LOG.debug('Starting background job %s', name)
        async def catch_cancel():
            try:
                await job()
            except asyncio.CancelledError:
                LOG.debug('Canceled background job %s', name)
        cor = catch_cancel()
        task = app.loop.create_task(cor)
        app[name] = task
        LOG.debug('Started  background job %s', name)

    async def on_shutdown(app):
        LOG.debug('Stopping background job %s', name)
        task = app[name]
        task.cancel()
        LOG.debug('Awaiting background job %s', name)
        await task
        LOG.debug('Stopped  background job %s', name)

    app.on_startup.append(on_startup)
    if not skip_shutdown:
        app.on_shutdown.append(on_shutdown)
